package selectionsortnamasiswa;

import static java.util.Arrays.sort;


class MergeSort
{

    private static void OIsam(int[] arr, int y, int h) {
        {
        if (y < h)
        {
            // Find the middle point
            int m = (y+h)/2;
 
            // Sort first and second halves
            sort(arr, y, m);
            sort(arr , m+1, h);
 
            // Merge the sorted halves
            merge(arr, y, m, h);
        }
    }
    }
    // Merges two subarrays of arr[].
    // First subarray is arr[l..m]
    // Second subarray is arr[m+1..r]
    static void merge(int arr[], int y, int m, int h)
    {
        // Find sizes of two subarrays to be merged
        int n1 = m - y + 1;
        int n2 = h - m;
 
        /* Create temp arrays */
        int L[] = new int [n1];
        int R[] = new int [n2];
 
        /*Copy data to temp arrays*/
        for (int i=0; i<n1; ++i)
            L[i] = arr[y + i];
        for (int j=0; j<n2; ++j)
            R[j] = arr[m + 1+ j];
 
 
        /* Merge the temp arrays */
 
        // Initial indexes of first and second subarrays
        int i = 0, j = 0;
 
        // Initial index of merged subarry array
        int k = y;
        while (i < n1 && j < n2)
        {
            if (L[i] <= R[j])
            {
                arr[k] = L[i];
                i++;
            }
            else
            {
                arr[k] = R[j];
                j++;
            }
            k++;
        }
 
        /* Copy remaining elements of L[] if any */
        while (i < n1)
        {
            arr[k] = L[i];
            i++;
            k++;
        }
 
        /* Copy remaining elements of R[] if any */
        while (j < n2)
        {
            arr[k] = R[j];
            j++;
            k++;
        }
    }
 
    // Main function that sorts arr[l..r] using
    // merge()
   
    /* A utility function to print array of size n */
    static void printArray(int arr[])
    {
        int n = arr.length;
        for (int i=0; i<n; ++i)
            System.out.print(arr[i] + " ");
        System.out.println();
    }
 
    // Driver method
    public static void main(String args[])
    {
        int arr[] = {12, 11, 13, 5, 6, 7};
 
        System.out.println("Data Sebelum di Sorting : ");
        printArray(arr);
 
        
        OIsam(arr, 0, arr.length-1);
 
        System.out.println("\nData Setelah di Sorting : ");
        printArray(arr);
    }
}
        


